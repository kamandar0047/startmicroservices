package com.muro.az.microstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrostartApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrostartApplication.class, args);
	}

}

