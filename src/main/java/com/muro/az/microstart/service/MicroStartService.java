package com.muro.az.microstart.service;

import com.muro.az.microstart.dto.MicroStartDTO;

public interface MicroStartService {
MicroStartDTO getMicroBYId( Long Id);
MicroStartDTO create(MicroStartDTO microStartDTO);
MicroStartDTO save(MicroStartDTO microStartDTO);

void delete(Long id);

}
