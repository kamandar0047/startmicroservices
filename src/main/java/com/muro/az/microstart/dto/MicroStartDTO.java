package com.muro.az.microstart.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MicroStartDTO {
 Long id;
 String name;
 String surname;
 String address;


}
