package com.muro.az.microstart.controller;

import com.muro.az.microstart.dto.MicroStartDTO;
import com.muro.az.microstart.service.MicroStartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

//microStart/id/1
//microStart/id?1&muro
@RestController
@RequestMapping(value = ("/microStart"))
@RequiredArgsConstructor
public class MicroStartController {

    private final MicroStartService microStartService;

    @GetMapping(value = "/id/{id}")
    public MicroStartDTO getMicroBYId(@PathVariable long id) {
        return microStartService.getMicroBYId(id);
    }

    @GetMapping(value = "/")
    public MicroStartDTO getMicroBYId2(@PathParam(value = "id") long id, String name) {
        return microStartService.getMicroBYId(id);
    }


}
